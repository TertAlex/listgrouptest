package com.sentiel.listgrouptest;

import java.util.ArrayList;

public class Group {
    public static String ID = "id";
    public static String NAME = "name";

    int id;
    String name;
    ArrayList<Item> items;


    public Group() {
        this.id = -1;
        this.name = "";
        this.items = new ArrayList<>();
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addItem(Item item){
        this.items.add(item);
    }

    public Item getItem(int id){
        return this.items.get(id);
    }

    public int getItemsSize(){
        return this.items.size();
    }
}
