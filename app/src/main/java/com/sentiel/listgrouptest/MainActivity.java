package com.sentiel.listgrouptest;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.ExpandableListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemLongClickListener {
    public static String GROUPS = "groups";
    public static String ITEMS = "items";
    public static final String TAG = "ListGroupTest_Logs";

    ArrayList<Group> groups;
    ExpandableListView expLVMain;
    ListAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String json_source = this.loadJSONFromAsset();
        if (json_source != null) {
            this.getGroupsFromJSON(json_source);
            this.getItemsFromJSON(json_source);
        }

        expLVMain = (ExpandableListView)findViewById(R.id.expLVMain);

        if (this.groups != null) {
            listAdapter = new ListAdapter(this, this.groups);
            if (expLVMain != null) {
                expLVMain.setAdapter(listAdapter);
                expLVMain.setOnItemLongClickListener(this);
            }
        }
    }


    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

        int itemType = ExpandableListView.getPackedPositionType(id);

        int groupPosition = ExpandableListView.getPackedPositionGroup(id);
        Group group = (Group)this.listAdapter.getGroup(groupPosition);

        if ( itemType == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
            int childPosition = ExpandableListView.getPackedPositionChild(id);
            Item item = (Item)this.listAdapter.getChild(groupPosition, childPosition);

            this.showItemDialog(group.getName(), item.getName());
            return true;

        } else if(itemType == ExpandableListView.PACKED_POSITION_TYPE_GROUP) {
            if ((group.getId() % 2) == 0) {
                this.startAnimation(true);
            }else {
                this.startAnimation(false);
            }
            return true;

        }else{
            return false;
        }
    }

    private void showItemDialog(String groupName, String childName) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(groupName)
                .setMessage(childName)
                .setCancelable(false)
                .setNegativeButton(getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void startAnimation(boolean isClockwise){
        Point displaySize = this.getDisplaySize();
        RotateAnimation rotate = new RotateAnimation(0, isClockwise ? 360 : -360,
                displaySize.x / 2, displaySize.y / 2);

        rotate.setDuration(1000);
        this.expLVMain.startAnimation(rotate);
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("data.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public void getGroupsFromJSON(String json_source) {
        try {
            JSONArray jsonArray = new JSONObject(json_source).getJSONArray(GROUPS);
            this.groups = new ArrayList<>(jsonArray.length());

            for (int i = 0; i < jsonArray.length(); i++) {
                Group group = ParseJSONGroup(jsonArray.getJSONObject(i));
                if (group != null) {
                    this.groups.add(group);  //group.getId(),
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getItemsFromJSON(String json_source) {
        try {
            JSONArray jsonArray = new JSONObject(json_source).getJSONArray(ITEMS);
            for (int i = 0; i < jsonArray.length(); i++) {
                Item item = ParseJSONItem(jsonArray.getJSONObject(i));
                if (item != null) {
                    for (Group group : this.groups) {
                        if (group.getId() == item.getGroup_id()){
                            group.addItem(item);
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Group ParseJSONGroup(JSONObject groupJSON) {
        Group group = new Group();
        try {
            group.setId(groupJSON.getInt(Group.ID));
            group.setName(groupJSON.getString(Group.NAME));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return group;
    }

    public Item ParseJSONItem(JSONObject itemJSON) {
        Item item = new Item();
        try {
            item.setId(itemJSON.getLong(Item.ID));
            item.setGroup_id(itemJSON.getInt(Item.GROUP_ID));
            item.setName(itemJSON.getString(Item.NAME));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return item;
    }

    private Point getDisplaySize() {
        Point size = new Point();
        WindowManager w = getWindowManager();
        w.getDefaultDisplay().getSize(size);
        return size;
    }
}
