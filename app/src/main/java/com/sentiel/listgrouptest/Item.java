package com.sentiel.listgrouptest;

public class Item {
    public static String ID = "id";
    public static String GROUP_ID = "group_id";
    public static String NAME = "name";


    long id;
    int group_id;
    String name;


    public Item() {
        this.id = -1;
        this.group_id = -4;
        this.name = "";
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getGroup_id() {
        return group_id;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
